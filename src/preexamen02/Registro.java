/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen02;

/**
 *
 * @author hp
 */
public class Registro {
    private int codigoVenta;
    private int cantidad;
    private String tipo;
    private float precio;
    
    
    public Registro(){
        this.codigoVenta = 0;
        this.cantidad = 0;
        this.tipo = "";
        this.precio = 0.0f; 
    }

    public Registro(int codigoVenta, int cantidad, String tipo, float precio) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }
    
    public Registro(Registro x){
        this.codigoVenta = x.codigoVenta;
        this.cantidad = x.cantidad;
        this.tipo = x.tipo;
        this.precio = x.precio;
    }
    

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    public float calcularCostoVenta(){
        float costoVenta = 0.0f;
        if (this.tipo .equals ("premium")){
            costoVenta = (this.cantidad * 18.50f);
        }
        else{
            costoVenta = (this.cantidad * 17.50f);
        }
        return costoVenta;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = (this.calcularCostoVenta()*0.16f);
        return impuesto;
    }
    public float calcularTotalPagar(){
        float totalPagar = 0.0f;
        totalPagar = (this.calcularCostoVenta()+ this.calcularImpuesto());
        return totalPagar;
    }
}
