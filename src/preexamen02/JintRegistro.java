/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen02;

import javax.swing.JOptionPane;

/**
 *
 * @author hp
 */
public class JintRegistro extends javax.swing.JInternalFrame {

    /**
     * Creates new form JintRegistro
     */
    public JintRegistro() {
        initComponents();
        this.resize(847,567);
        this.deshabilitar();
    }
    public void deshabilitar(){
        this.txtCodigo.setEnabled(false);
        this.txtCantidad.setEnabled(false);
        this.txtCosto.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.txtTotal.setEnabled(false);
        this.cmbTipo.setEnabled(false);
        
        //botones
        this.btnCalcular.setEnabled(false);
        this.btnGuardar.setEnabled(false);  
    }
    public void habilitar(){
        this.txtCodigo.setEnabled(!false);
        this.txtCantidad.setEnabled(!false);
        this.btnGuardar.setEnabled(!false);
         this.cmbTipo.setEnabled(!false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        txtCodigo = new javax.swing.JTextField();
        cmbTipo = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtCosto = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        btnCalcular = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 204));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Registro de Ventas de gasolina");
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Cantidad :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(80, 100, 100, 50);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Codigo de Venta :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 40, 160, 50);

        txtCantidad.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        getContentPane().add(txtCantidad);
        txtCantidad.setBounds(190, 110, 190, 40);

        txtCodigo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        getContentPane().add(txtCodigo);
        txtCodigo.setBounds(190, 50, 190, 40);

        cmbTipo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tipo de gasolina", "premium", "regular" }));
        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoActionPerformed(evt);
            }
        });
        getContentPane().add(cmbTipo);
        cmbTipo.setBounds(190, 180, 190, 40);

        jPanel1.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Costo de Venta :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 10, 160, 50);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Precio :");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(320, 20, 80, 50);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("Impuesto :");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(50, 60, 100, 50);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("Total a pagar :");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(20, 120, 130, 50);

        txtCosto.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jPanel1.add(txtCosto);
        txtCosto.setBounds(160, 20, 130, 40);

        txtPrecio.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jPanel1.add(txtPrecio);
        txtPrecio.setBounds(400, 30, 130, 40);

        txtImpuesto.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jPanel1.add(txtImpuesto);
        txtImpuesto.setBounds(160, 70, 130, 40);

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jPanel1.add(txtTotal);
        txtTotal.setBounds(160, 130, 130, 40);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(60, 240, 560, 200);

        btnCalcular.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnCalcular.setText("Calcular");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        getContentPane().add(btnCalcular);
        btnCalcular.setBounds(640, 210, 160, 70);

        btnNuevo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(640, 50, 160, 70);

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(640, 130, 160, 70);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
        // TODO add your handling code here:
        //mostrar la informacion
        
        this.txtCodigo.setText(String.valueOf(con.getCodigoVenta()));
        this.txtCantidad.setText(String.valueOf(con.getCantidad()));
        String Tipo = con.getTipo();
        if (this.cmbTipo.getSelectedItem().equals("premium") ){
            this.cmbTipo.setSelectedItem(1);
            //this.txtPrecio = 18.50f;
        }
        if (this.cmbTipo.getSelectedItem().equals("regular") ){
            this.cmbTipo.setSelectedItem(2);
            
        }
        //mostrar los resultados
        this.txtCosto.setText(String.valueOf(con.calcularCostoVenta()));
        this.txtImpuesto.setText(String.valueOf(con.calcularImpuesto()));
        this.txtTotal.setText(String.valueOf(con.calcularTotalPagar()));
        this.txtPrecio.setText(String.valueOf(con.getPrecio()));
        //this.txtPrecio.setText(String.valueOf(()));


        
        
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void cmbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbTipoActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        con = new Registro();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
         //hacer una validacion
        boolean exito = false;
            if (this.txtCantidad.getText().equals(""))exito = true;
            if (this.txtCodigo.getText().equals(""))exito = true;
            if (this.cmbTipo.getSelectedItem().equals("Tipo de gasolina"))exito = true;
            if (exito == true){
                //falto informacion
                 JOptionPane.showMessageDialog(this, "falto capturar informacion");
            }
            else{
                //todo correcto
                con.setCodigoVenta(Integer.parseInt(this.txtCodigo.getText()));
                con.setCantidad(Integer.parseInt(this.txtCantidad.getText()));
                con.setTipo(String.valueOf(this.cmbTipo.getSelectedItem()));
                if (con.getTipo().equals("premium"))con.setPrecio(18.50f);
                else con.setPrecio(17.50f);
                
                JOptionPane.showMessageDialog(this, "se guardo con exito la informacion");
                this.btnCalcular.setEnabled(true);
            }
        
    
        
        
    }//GEN-LAST:event_btnGuardarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtCosto;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
private Registro con;
}
